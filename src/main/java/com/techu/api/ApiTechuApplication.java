package com.techu.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiTechuApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiTechuApplication.class, args);
	}

}
